package edu.personal.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import edu.personal.beans.Candidate;
import edu.personal.beans.Employee;
import edu.personal.beans.FileUpload;
import edu.personal.beans.Profile;
import edu.personal.beans.VacancyRequest;
import edu.personal.common.CandidateStatus;
import edu.personal.dao.RecruitmentRepositoryImpl;
import edu.personal.util.SendByEmail;

@Service
public class RecruitmentService {

	private static final Logger logger = LoggerFactory
			.getLogger(RecruitmentService.class);

	@Autowired
	RecruitmentRepositoryImpl recruitmentRepositoryImpl;

	public String getProfile(String userId, String password) {
		return recruitmentRepositoryImpl.getProfile(userId, password);
	}

	public List<String> getProfiles() {
		return recruitmentRepositoryImpl.getProfiles();
	}

	public List<Candidate> getCandidates() {
		return recruitmentRepositoryImpl.getCandidates();
	}

	public List<Employee> getEmployees() {
		return recruitmentRepositoryImpl.getEmployees();
	}

	public Candidate getCandidate(String candidateId) {
		return recruitmentRepositoryImpl.getCandidate(candidateId);
	}

	public List<VacancyRequest> getVacanciesForEmployee(String userId) {
		Profile profile = recruitmentRepositoryImpl.getProfileByUserID(userId
				.toUpperCase());
		if ("2001".equals(profile.getRoleId())) {
			return recruitmentRepositoryImpl.getVacanciesForEmployee(Integer
					.parseInt(profile.getEmployeeId()));
		}
		return null;
	}

	public List<VacancyRequest> getVacanciesForHR() {
		return recruitmentRepositoryImpl.getVacanciesForHR();
	}

	public List<VacancyRequest> getVacanciesForUnitHead(String status) {
		return recruitmentRepositoryImpl.getVacanciesForUnitHead(status);
	}

	public List<Candidate> getCandidatesForUnitHead(String status) {
		return recruitmentRepositoryImpl.getCandidatesForUnitHead(status);
	}

	public VacancyRequest saveVacancyRequest(VacancyRequest vacancyRequest) {
		return recruitmentRepositoryImpl.saveVacancyRequest(vacancyRequest);
	}

	public Candidate saveCandidate(Candidate candidate) {
		return recruitmentRepositoryImpl.saveCandidate(candidate);
	}

	public Candidate saveMedicalResultByHR(Candidate candidate) {
		Candidate c = recruitmentRepositoryImpl
				.saveMedicalResultByHR(candidate);
		if (c.getCandidateEmail() != null && !c.getCandidateEmail().isEmpty()) {
			SendByEmail
					.sendEmail(
							"Congratulations You have successfully cleared Medical Tests, Now you are an Employee of ABC Company",
							c.getCandidateEmail(),
							"ABC Company - Medical Tests Status ");
		}
		return c;
	}

	public void removeVacancyRequest(VacancyRequest vacancyRequest) {
		recruitmentRepositoryImpl.deleteVacancyRequest(vacancyRequest);
	}

	public void apprOrRejUnitHeadCandidate(Candidate candidate) {
		String actionType = candidate.getActionType();
		String candidateEmail = candidate.getCandidateEmail();
		if (actionType.equalsIgnoreCase(CandidateStatus.PASSED
				.toString())){
			SendByEmail.sendEmail("Congratulations You successfully cleared your Interview",candidateEmail,"ABC Company - Interview Status ");
		}
		recruitmentRepositoryImpl.apprOrRejUnitHeadCandidate(candidate);
	}

	public void fileUpload(FileUpload uploadedFile) {
		InputStream inputStream = null;
		try {
			MultipartFile file = uploadedFile.getFile();
			inputStream = file.getInputStream();
			String filePath = System.getProperty("catalina.home") + "/Resumes/";
			System.out.println("Server path:" + filePath);
			File fileToCreate = new File(filePath, file.getOriginalFilename());
			FileUtils.copyToFile(inputStream, fileToCreate);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}