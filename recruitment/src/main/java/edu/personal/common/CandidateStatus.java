package edu.personal.common;

public enum CandidateStatus {
	
	APPROVED("Approved"),
	REJECTED("Rejected"),
	PASSED("Passed"),
	FAILED("Failed");
	
	private String status;
	
	CandidateStatus(String status){
		this.status = status;
	}

}
