package edu.personal.common;

import java.util.List;

import edu.personal.beans.Candidate;
import edu.personal.beans.Employee;
import edu.personal.beans.Role;
import edu.personal.beans.VacancyRequest;

public class ResponseInfo {
	private String status;
	private String errorMessage;
	private String errorCode;
	private Employee employee;
	private Candidate candidate;
	private Role role;
	private List<Employee> employeeList;
	private List<String> roleTypeList;
	private List<VacancyRequest> vacancyList;
	private List<Integer> candidateIdList;
	private List<Candidate> candidateList;

	public List<Candidate> getCandidateList() {
		return candidateList;
	}

	public void setCandidateList(List<Candidate> candidateList) {
		this.candidateList = candidateList;
	}

	public List<Integer> getCandidateIdList() {
		return candidateIdList;
	}

	public void setCandidateIdList(List<Integer> candidateIdList) {
		this.candidateIdList = candidateIdList;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<String> getRoleTypeList() {
		return roleTypeList;
	}

	public void setRoleTypeList(List<String> roleTypeList) {
		this.roleTypeList = roleTypeList;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<VacancyRequest> getVacancyList() {
		return vacancyList;
	}

	public void setVacancyList(List<VacancyRequest> vacancyList) {
		this.vacancyList = vacancyList;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

}