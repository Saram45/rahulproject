package edu.personal.common;

/**
 * 
 * @author rahul
 *
 */
public class Constants {

	// Status Message and Error codes
	public static final String SUCCESS = "success";
	public static final String FAILURE = "failure";
	public static final String SUCCESS_ERROR_CD = "200";
	public static final String SUCCESS_ERROR_MSG = "Transaction has been completed successfully";
	public static final String FAILURE_ERROR_CD = "500";
	public static final String saveErrorMessage = "Could not be saved";
	
	//E-Mail props
	public static final String SMTP_HOST="mail.smtp.host";
	public static final String SMTP_PORT="mail.smtp.port";
	public static final String SMTP_AUTH="mail.smtp.auth";
	public static final String SMTP_ENABLE="mail.smtp.starttls.enable";
	public static final String SMTP_MAIL_FROM="mail.from";
	public static final String RECRUITMENT_ADMINID="rahulpidvs04@gmail.com";
	public static final String RECRUITMENT_ADMINPWD="Re@dy123";
		
	public static final String uploadFolder = "/app/Rahul/";
	
	

}