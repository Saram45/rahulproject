package edu.personal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import edu.personal.beans.Candidate;
import edu.personal.beans.Employee;
import edu.personal.beans.Profile;
import edu.personal.beans.VacancyRequest;
import edu.personal.common.CandidateStatus;

/**
 * 
 * @author Rahul
 *
 */
@Repository("recruitmentRepositoryImpl")
@Transactional
public class RecruitmentRepositoryImpl {

	private static final Logger logger = LoggerFactory
			.getLogger(RecruitmentRepositoryImpl.class);

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public List<String> getProfiles() {
		List<String> roleTypeList = new ArrayList<String>();
		String sql = "SELECT ROLE_TYPE FROM ROLES";
		Connection conn = null;

		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			String roleName = null;
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				roleName = rs.getString("ROLE_TYPE");
				roleTypeList.add(roleName);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			logger.error("SQLException while fetching MODEL : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("SQLException while fetching MODEL: "
							+ e.getMessage());
				}
			}
		}
		return roleTypeList;
	}

	public List<Candidate> getCandidates() {
		List<Candidate> candidateList = new ArrayList<Candidate>();

		String sql = "SELECT * FROM CANDIDATE where APPROVAL_STATUS = 'Approved' and INTERVIEW_CLEARED = 'Passed' and MEDICAL_CLEARED = 'Pending'";
		Connection conn = null;

		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Candidate candidate = new Candidate();
				candidate.setCandidateId(String.valueOf(rs.getInt("CANDIDATE_ID")));
				candidate.setVacancyReqId(String.valueOf(rs.getInt("VACANCY_REQ_ID")));
				candidate.setCandidateName(rs.getString("CANDIDATE_NAME"));
				candidate.setCandidateAge(String.valueOf(rs.getInt("AGE")));
				candidate.setCandidateSex(rs.getString("SEX"));
				candidate.setCandidateEmail(rs.getString("EMAIL_ID"));
				candidate.setResume(rs.getString("RESUME"));
				candidate.setApprovalStatus(rs.getString("APPROVAL_STATUS"));
				candidate.setInterviewCleared(rs.getString("INTERVIEW_CLEARED"));
				candidate.setMedicalCleared(rs.getString("MEDICAL_CLEARED"));
				
				candidateList.add(candidate);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			logger.error("SQLException while fetching MODEL : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("SQLException while fetching MODEL: "
							+ e.getMessage());
				}
			}
		}
		return candidateList;
	}

	public List<Candidate> getCandidatesForUnitHead(String status) {
		List<Candidate> candidateList = new ArrayList<Candidate>();
		String sql = "";
		if ("Approved".equals(status)) {
			sql = "select * from candidate where APPROVAL_STATUS = 'Approved' and INTERVIEW_CLEARED = 'Pending'";
		} else {
			sql = "SELECT * FROM candidate where APPROVAL_STATUS = 'Pending'";
		}
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Candidate candidate = new Candidate();
				
				candidate.setCandidateId(String.valueOf(rs.getInt("CANDIDATE_ID")));
				candidate.setVacancyReqId(String.valueOf(rs.getInt("VACANCY_REQ_ID")));
				candidate.setCandidateName(rs.getString("CANDIDATE_NAME"));
				candidate.setCandidateAge(String.valueOf(rs.getInt("AGE")));
				candidate.setCandidateSex(rs.getString("SEX"));
				candidate.setCandidateEmail(rs.getString("EMAIL_ID"));
				candidate.setResume(rs.getString("RESUME"));
				candidate.setApprovalStatus(rs.getString("APPROVAL_STATUS"));
				candidate.setInterviewCleared(rs.getString("INTERVIEW_CLEARED"));
				candidate.setMedicalCleared(rs.getString("MEDICAL_CLEARED"));
				
				candidateList.add(candidate);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			logger.error("SQLException while fetching MODEL : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("SQLException while fetching MODEL: "
							+ e.getMessage());
				}
			}
		}
		return candidateList;
	}

	public Candidate getCandidate(String candidateId) {
		Candidate candidate = new Candidate();
		int candidateNo = Integer.parseInt(candidateId);
		String sql = "SELECT * FROM CANDIDATE where candidate_id = "
				+ candidateNo;
		Connection conn = null;

		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				candidate.setCandidateId(String.valueOf(rs.getInt("CANDIDATE_ID")));
				candidate.setVacancyReqId(String.valueOf(rs.getInt("VACANCY_REQ_ID")));
				candidate.setCandidateName(rs.getString("CANDIDATE_NAME"));
				candidate.setCandidateAge(String.valueOf(rs.getInt("AGE")));
				candidate.setCandidateSex(rs.getString("SEX"));
				candidate.setCandidateEmail(rs.getString("EMAIL_ID"));
				candidate.setResume(rs.getString("RESUME"));
				candidate.setApprovalStatus(rs.getString("APPROVAL_STATUS"));
				candidate.setInterviewCleared(rs.getString("INTERVIEW_CLEARED"));
				candidate.setMedicalCleared(rs.getString("MEDICAL_CLEARED"));
				
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			logger.error("SQLException while fetching MODEL : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("SQLException while fetching MODEL: "
							+ e.getMessage());
				}
			}
		}
		return candidate;
	}

	public String getProfile(String userId, String password) {
		String roleName = null;
		String sql = "select roles.ROLE_TYPE FROM roles,employee WHERE roles.ROLE_ID = employee.ROLE_ID and employee.EMPLOYEE_NAME = ? and employee.PASSWORD = ? ";
		Connection conn = null;

		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, userId);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				roleName = rs.getString("ROLE_TYPE");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			logger.error("SQLException while fetching MODEL : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("SQLException while fetching MODEL: "
							+ e.getMessage());
				}
			}
		}
		return roleName;
	}

	public Profile getProfileByUserID(String userId) {
		String userID = userId.trim();
		;
		String sql = "select * FROM employee WHERE employee.EMPLOYEE_NAME = '"
				+ userID + "'";
		Connection conn = null;
		Profile profile = new Profile();
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {

				profile.setRoleId(rs.getString("ROLE_ID"));
				profile.setEmployeeId(rs.getString("EMPLOYEE_ID"));

			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			logger.error("SQLException while fetching MODEL : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("SQLException while fetching MODEL: "
							+ e.getMessage());
				}
			}
		}
		return profile;
	}

	public VacancyRequest saveVacancyRequest(VacancyRequest vacancyRequest) {
		String InsertSql = "INSERT INTO vacancy_request "
				+ "(VACANCY_REQ_ID,ROLE_TYPE,LOCATION,EXPERIENCE,SKILLS,CREATED_BY) VALUES (?,?,?,?,?,?)";
		String getMaxReqId = "select max(vacancy_req_id) from vacancy_request";
		String getCreatedBYId = "select employee_Id from employee where EMPLOYEE_NAME = '"
				+ vacancyRequest.getCreatedBy().trim() + "'";
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps0 = conn.prepareStatement(getMaxReqId);
			ResultSet rs0 = ps0.executeQuery();
			int nextVacReqId = 0;
			if (rs0.next()) {
				nextVacReqId = rs0.getInt(1);
			}

			PreparedStatement ps1 = conn.prepareStatement(getCreatedBYId);
			ResultSet rs1 = ps1.executeQuery();
			int createdById = 0;
			if (rs1.next()) {
				createdById = rs1.getInt(1);
			}

			PreparedStatement ps = conn.prepareStatement(InsertSql);
			ps.setInt(1, nextVacReqId + 1);
			ps.setString(2, vacancyRequest.getSelectedRoleType());
			ps.setString(3, vacancyRequest.getLocation());
			ps.setInt(4, Integer.parseInt(vacancyRequest.getExperience().trim()));
			ps.setString(5, vacancyRequest.getSkill());
			ps.setInt(6, createdById);
			ps.executeUpdate();

			ps.close();

		} catch (SQLException e) {
			logger.error("SQLException while saving profile selection rules : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
					return vacancyRequest;
				} catch (SQLException e) {
					logger.error("SQLException while saving profile selection rules : "
							+ e.getMessage());
				}
			}

		}
		return vacancyRequest;

	}

	public Candidate saveCandidate(Candidate candidate) {
		String InsertSql = "INSERT INTO CANDIDATE "
				+ "(CANDIDATE_ID, CANDIDATE_NAME, AGE,SEX,EMAIL_ID,VACANCY_REQ_ID,RESUME) VALUES (?,?,?,?,?,?,?)";
		String getMaxCandidateId = "select max(CANDIDATE_ID) from CANDIDATE";
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps0 = conn.prepareStatement(getMaxCandidateId);
			ResultSet rs0 = ps0.executeQuery();
			int nextCandId = 0;
			if (rs0.next()) {
				nextCandId = rs0.getInt(1);
			}

			PreparedStatement ps = conn.prepareStatement(InsertSql);
			ps.setInt(1, nextCandId + 1);
			ps.setString(2, candidate.getCandidateName().trim());
			ps.setInt(3, Integer.parseInt(candidate.getCandidateAge().trim()));
			ps.setString(4, candidate.getCandidateSex().trim());
			ps.setString(5, candidate.getCandidateEmail().trim());
			ps.setInt(6, Integer.parseInt(candidate.getVacancyReqId().trim()));
			ps.setString(7, candidate.getResume().trim());
			ps.executeUpdate();

			ps.close();

		} catch (SQLException e) {
			logger.error("SQLException while saving Candidate  : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
					return candidate;
				} catch (SQLException e) {
					logger.error("SQLException while saving candidate"
							+ e.getMessage());
				}
			}

		}
		return candidate;

	}

	public Candidate saveMedicalResultByHR(Candidate candidate) {
		int candidateId = Integer.parseInt(candidate.getCandidateId());
		String isCleared = candidate.getMedicalCleared();
		
		String saveMedicalResultSql = "update CANDIDATE set MEDICAL_CLEARED= '"
				+ isCleared + "' where CANDIDATE_ID = " + candidateId;
		
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps;
			if (isCleared.equals("Yes")) {

				ps = conn.prepareStatement(saveMedicalResultSql);
				ps.executeUpdate();
				
				String getCandidateByIdSql = "SELECT * FROM candidate where CANDIDATE_ID = " + candidateId;
				ps = conn.prepareStatement(getCandidateByIdSql);
				ResultSet rs = ps.executeQuery();
				Candidate candidateFromDB =  new Candidate();
				if (rs.next()) {
					candidateFromDB.setVacancyReqId(rs.getString("VACANCY_REQ_ID"));
					candidateFromDB.setCandidateName(rs.getString("CANDIDATE_NAME"));
					candidateFromDB.setCandidateAge(String.valueOf(rs.getInt("AGE")));
					candidateFromDB.setCandidateSex(rs.getString("SEX"));
					candidateFromDB.setCandidateEmail(rs.getString("EMAIL_ID"));
				}
				
				candidate.setCandidateEmail(candidateFromDB.getCandidateEmail());
				
				String getRoleIdForVacancyReqIdSql = "select DISTINCT r.role_id from roles r, candidate c, vacancy_request vr where vr.VACANCY_REQ_ID = c.VACANCY_REQ_ID and vr.ROLE_TYPE= r.ROLE_TYPE and c.VACANCY_REQ_ID = "
						+ Integer.parseInt(candidateFromDB.getVacancyReqId());
				ps = conn.prepareStatement(getRoleIdForVacancyReqIdSql);
				rs = ps.executeQuery();
				int roleId = 0;
				if (rs.next()) {
					roleId = rs.getInt(1);
				}

				String getMaxEmpIdSql = "SELECT max(EMPLOYEE_ID) FROM employee";
				ps = conn.prepareStatement(getMaxEmpIdSql);
				rs = ps.executeQuery();
				int empId = 0;
				if (rs.next()) {
					empId = rs.getInt(1);
				}

				String insertIntoEmployeeSql = "INSERT INTO EMPLOYEE "
						+ "(EMPLOYEE_ID,ROLE_ID,EMPLOYEE_NAME,AGE,SEX) VALUES (?,?,?,?,?)";
				ps = conn.prepareStatement(insertIntoEmployeeSql);
				ps.setInt(1, empId + 1);
				ps.setInt(2, roleId);
				ps.setString(3, candidateFromDB.getCandidateName().trim());
				ps.setInt(4,
						Integer.parseInt(candidateFromDB.getCandidateAge().trim()));
				ps.setString(5, candidateFromDB.getCandidateSex().trim());
				ps.executeUpdate();

				String updateVacReqStatusSql = "update VACANCY_REQUEST set STATUS = 'Filled' where VACANCY_REQ_ID = "
						+ Integer.parseInt(candidateFromDB.getVacancyReqId());
				ps = conn.prepareStatement(updateVacReqStatusSql);
				ps.executeUpdate();
				
			} else {
				ps = conn.prepareStatement(saveMedicalResultSql);
				ps.executeUpdate();
			}
			ps.close();
		} catch (SQLException e) {
			logger.error("SQLException while saving Candidate  : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
					return candidate;
				} catch (SQLException e) {
					logger.error("SQLException while saving candidate"
							+ e.getMessage());
				}
			}
		}
		return candidate;
	}

	public void deleteVacancyRequest(VacancyRequest vacancyRequest) {
		String vacancyReqId = vacancyRequest.getVacancyReqId();
		String sql = "delete from vacancy_request where VACANCY_REQ_ID = '"
				+ vacancyReqId + "'";

		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.executeUpdate();

			ps.close();

		} catch (SQLException e) {
			logger.error("SQLException while saving profile selection rules : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("SQLException while saving profile selection rules : "
							+ e.getMessage());
				}
			}
		}

	}

	public void apprOrRejUnitHeadCandidate(Candidate candidate) {
		String candidateId = candidate.getCandidateId();
		String actionType = candidate.getActionType();
		String sql = "";
		if (actionType.equalsIgnoreCase(CandidateStatus.APPROVED.toString())
				|| actionType.equalsIgnoreCase(CandidateStatus.REJECTED
						.toString())) {
			sql = "update CANDIDATE set APPROVAL_STATUS= '"
					+ candidate.getActionType() + "'"
					+ " where CANDIDATE_ID = '" + candidateId + "'";
		} else if (actionType.equalsIgnoreCase(CandidateStatus.PASSED
				.toString())
				|| actionType.equalsIgnoreCase(CandidateStatus.FAILED
						.toString())) {
			sql = "update CANDIDATE set INTERVIEW_CLEARED= '"
					+ candidate.getActionType() + "'"
					+ " where CANDIDATE_ID = '" + candidateId + "'";
		}
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			logger.error("SQLException while saving profile selection rules : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("SQLException while saving profile selection rules : "
							+ e.getMessage());
				}
			}
		}
	}

	public List<VacancyRequest> getVacanciesForHR() {
		List<VacancyRequest> vacancyList = new ArrayList<VacancyRequest>();
		String sql = "select * from vacancy_request where STATUS <> 'Filled'";
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				VacancyRequest vacancyRequest = new VacancyRequest();
				
				vacancyRequest.setVacancyReqId(rs.getString("VACANCY_REQ_ID"));
				vacancyRequest.setSelectedRoleType(rs.getString("ROLE_TYPE"));
				vacancyRequest.setLocation(rs.getString("LOCATION"));
				vacancyRequest.setExperience(rs.getString("EXPERIENCE"));
				vacancyRequest.setSkill(rs.getString("SKILLS"));
				vacancyRequest.setCreatedBy(rs.getString("CREATED_BY"));
				vacancyRequest.setStatus(rs.getString("STATUS"));
				
				vacancyList.add(vacancyRequest);
			}
			rs.close();
			ps.close();

		} catch (SQLException e) {
			logger.error("SQLException while saving profile selection rules : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("SQLException while saving profile selection rules : "
							+ e.getMessage());
				}
			}
		}
		return vacancyList;
	}

	public List<VacancyRequest> getVacanciesForEmployee(int employeeId) {
		List<VacancyRequest> vacancyList = new ArrayList<VacancyRequest>();

		String sql = "select * from vacancy_request where status <> 'Filled' and created_by = '"
				+ employeeId + "'";
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				VacancyRequest vacancyRequest = new VacancyRequest();
				
				vacancyRequest.setVacancyReqId(rs.getString("VACANCY_REQ_ID"));
				vacancyRequest.setSelectedRoleType(rs.getString("ROLE_TYPE"));
				vacancyRequest.setLocation(rs.getString("LOCATION"));
				vacancyRequest.setExperience(rs.getString("EXPERIENCE"));
				vacancyRequest.setSkill(rs.getString("SKILLS"));
				vacancyRequest.setCreatedBy(rs.getString("CREATED_BY"));
				vacancyRequest.setStatus(rs.getString("STATUS"));
				
				vacancyList.add(vacancyRequest);
			}
			rs.close();
			ps.close();

		} catch (SQLException e) {
			logger.error("SQLException while saving profile selection rules : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("SQLException while saving profile selection rules : "
							+ e.getMessage());
				}
			}
		}
		return vacancyList;
	}

	public List<VacancyRequest> getVacanciesForUnitHead(String status) {
		List<VacancyRequest> vacancyList = new ArrayList<VacancyRequest>();
		String sql = "select * from vacancy_request where STATUS <> 'Filled'";
		
		 
		 

		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				VacancyRequest vacancyRequest = new VacancyRequest();
				
				vacancyRequest.setVacancyReqId(rs.getString("VACANCY_REQ_ID"));
				vacancyRequest.setSelectedRoleType(rs.getString("ROLE_TYPE"));
				vacancyRequest.setLocation(rs.getString("LOCATION"));
				vacancyRequest.setExperience(rs.getString("EXPERIENCE"));
				vacancyRequest.setSkill(rs.getString("SKILLS"));
				vacancyRequest.setCreatedBy(rs.getString("CREATED_BY"));
				vacancyRequest.setStatus(rs.getString("STATUS"));
				
				vacancyList.add(vacancyRequest);
			}
			rs.close();
			ps.close();

		} catch (SQLException e) {
			logger.error("SQLException while saving profile selection rules : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("SQLException while saving profile selection rules : "
							+ e.getMessage());
				}
			}
		}
		return vacancyList;
	}


	public List<Employee> getEmployees() {
		List<Employee> employeeList = new ArrayList<Employee>();
		String sql = "SELECT * FROM Employee";
		Connection conn = null;

		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Employee employee = new Employee();
				
				employee.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
				employee.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
				employee.setRoleId(rs.getInt("ROLE_ID"));
				employee.setAge(rs.getInt("AGE"));
				employee.setSex(rs.getString("SEX"));
				employee.setPassword(rs.getString("PASSWORD"));
				
				employeeList.add(employee);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			logger.error("SQLException while fetching MODEL : "
					+ e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("SQLException while fetching MODEL: "
							+ e.getMessage());
				}
			}
		}
		return employeeList;
	}
}