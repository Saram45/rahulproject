package edu.personal.util;
 
 
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.personal.common.Constants;
 
	
	public class SendByEmail { 

		private static final Logger logger = LoggerFactory.getLogger(SendByEmail.class);
		
		public static boolean sendEmail(String mailBody,String receipientEmailId,String subject){
			boolean isSent = false;
			Properties properties = getMailProperties(); 
			Session session = Session.getDefaultInstance(properties,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(Constants.RECRUITMENT_ADMINID,Constants.RECRUITMENT_ADMINPWD);
				}
			});
			try {
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(Constants.RECRUITMENT_ADMINID)); 
				Calendar cal = Calendar.getInstance();
				message.setSubject(subject +  cal.getTime()); 
				List<String> recipientList = new ArrayList<String>();
				recipientList.add(receipientEmailId);
		    	InternetAddress[] recipientAddress = new InternetAddress[recipientList.size()];
				int counter = 0;
				for (String recipient : recipientList) {
				    recipientAddress[counter] = new InternetAddress(recipient.trim());
				    counter++;
				}
				message.setRecipients(Message.RecipientType.TO, recipientAddress); 
				
				message.setContent(mailBody, "text/html");
				message.setSentDate(new Date());
				Transport.send(message);
				isSent = true;
				logger.info(" [ isSent is = " + isSent + "] ");
			} catch (MessagingException mex) {
				logger.info(" [ exception in email sending is = " + mex+ "] ");
				logger.error("exception in email sending: " + mex);
				isSent = false;
				logger.info(" [ isSent is = " + isSent + "] ");
			}
			return isSent;
		} 
		
		/**
		 * 	
		 * @param propMap
		 * @return
		 */
			public static Properties getMailProperties() {
				// sets SMTP server properties
				Properties properties = new Properties(); 
				properties.put(Constants.SMTP_HOST,"smtp.gmail.com");
				properties.put(Constants.SMTP_PORT,Integer.valueOf("465"));
				//properties.put(Constants.SMTP_AUTH,sysToolProperties.getValue(Constants.SMTP_AUTH));
				properties.put("mail.smtp.socketFactory.class",	"javax.net.ssl.SSLSocketFactory");
				//properties.put(Constants.SMTP_ENABLE,"false");
				//properties.put(Constants.SMTP_MAIL_FROM, "sarat.kasa@gmail.com"); 
				properties.put("mail.smtp.auth", "true");
				properties.put("mail.smtp.socketFactory.port", "465");
			
				return properties;

			}
	}
