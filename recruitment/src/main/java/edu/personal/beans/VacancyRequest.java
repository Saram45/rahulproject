package edu.personal.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class VacancyRequest {

	private String vacancyReqId;
	private String selectedRoleType;
	private String location;
	private String skill;
	private String experience;
	private String createdBy;
	private String status;
	
	private String actionType;

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getSelectedRoleType() {
		return selectedRoleType;
	}

	public void setSelectedRoleType(String selectedRoleType) {
		this.selectedRoleType = selectedRoleType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getVacancyReqId() {
		return vacancyReqId;
	}

	public void setVacancyReqId(String vacancyReqId) {
		this.vacancyReqId = vacancyReqId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}