package edu.personal.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Candidate {

	private String candidateId;
	private String vacancyReqId;
	private String candidateName;
	private String candidateAge;
	private String candidateSex;
	private String candidateEmail;
	private String resume;
	private String approvalStatus;
	private String interviewCleared;
	private String medicalCleared;
	
	private String actionType;

	public String getCandidateEmail() {
		return candidateEmail;
	}

	public void setCandidateEmail(String candidateEmail) {
		this.candidateEmail = candidateEmail;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getVacancyReqId() {
		return vacancyReqId;
	}

	public void setVacancyReqId(String vacancyReqId) {
		this.vacancyReqId = vacancyReqId;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public String getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public String getInterviewCleared() {
		return interviewCleared;
	}

	public void setInterviewCleared(String interviewCleared) {
		this.interviewCleared = interviewCleared;
	}

	public String getMedicalCleared() {
		return medicalCleared;
	}

	public void setMedicalCleared(String medicalCleared) {
		this.medicalCleared = medicalCleared;
	}

	public String getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(String candidateId) {
		this.candidateId = candidateId;
	}

	public String getCandidateName() {
		return candidateName;
	}

	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}

	public String getCandidateAge() {
		return candidateAge;
	}

	public void setCandidateAge(String candidateAge) {
		this.candidateAge = candidateAge;
	}

	public String getCandidateSex() {
		return candidateSex;
	}

	public void setCandidateSex(String candidateSex) {
		this.candidateSex = candidateSex;
	}

}