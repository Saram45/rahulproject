package edu.personal.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import edu.personal.beans.Candidate;
import edu.personal.beans.Employee;
import edu.personal.beans.FileUpload;
import edu.personal.beans.Role;
import edu.personal.beans.VacancyRequest;
import edu.personal.common.Constants;
import edu.personal.common.ResponseInfo;
import edu.personal.service.RecruitmentService;

@MultipartConfig(fileSizeThreshold = 20971520)
@RestController
public class RecruitmentController {

	private static final Logger logger = LoggerFactory
			.getLogger(RecruitmentController.class);

	@Autowired
	RecruitmentService recruitmentService;

	@Autowired
	HttpSession session;

	@RequestMapping(value = "getProfile/{userId}/{password}", method = RequestMethod.GET)
	public ResponseEntity<ResponseInfo> getProfile(@PathVariable String userId,
			@PathVariable String password) {
		ResponseInfo responseInfo = new ResponseInfo();
		String roleType = recruitmentService.getProfile(userId, password);

		Role role = new Role();
		role.setRoleType(roleType);
		role.setUserId(userId);
		role.setPassword(password);

		responseInfo.setRole(role);
		responseInfo.setStatus(Constants.SUCCESS);
		responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
		responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);

		return new ResponseEntity<ResponseInfo>(responseInfo, HttpStatus.OK);
	}

	@RequestMapping(value = "getProfiles", method = RequestMethod.GET)
	public ResponseEntity<ResponseInfo> getProfiles() {
		ResponseInfo responseInfo = new ResponseInfo();
		List<String> roleTypeList = recruitmentService.getProfiles();
		responseInfo.setRoleTypeList(roleTypeList);
		responseInfo.setStatus(Constants.SUCCESS);
		responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
		responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
		return new ResponseEntity<ResponseInfo>(responseInfo, HttpStatus.OK);
	}

	@RequestMapping(value = "getCandidates", method = RequestMethod.GET)
	public ResponseEntity<ResponseInfo> getCandidates() {
		ResponseInfo responseInfo = new ResponseInfo();
		List<Candidate> candidateList = recruitmentService.getCandidates();
		responseInfo.setCandidateList(candidateList);
		responseInfo.setStatus(Constants.SUCCESS);
		responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
		responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
		return new ResponseEntity<ResponseInfo>(responseInfo, HttpStatus.OK);
	}

	@RequestMapping(value = "getEmployees", method = RequestMethod.GET)
	public ResponseEntity<ResponseInfo> getEmployees() {
		ResponseInfo responseInfo = new ResponseInfo();
		List<Employee> employeeList = recruitmentService.getEmployees();
		responseInfo.setEmployeeList(employeeList);
		responseInfo.setStatus(Constants.SUCCESS);
		responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
		responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
		return new ResponseEntity<ResponseInfo>(responseInfo, HttpStatus.OK);
	}

	

	@RequestMapping(value = "getCandidate/{candidateId}", method = RequestMethod.GET)
	public ResponseEntity<ResponseInfo> getCandidate(
			@PathVariable String candidateId) {
		ResponseInfo responseInfo = new ResponseInfo();
		Candidate candidate = recruitmentService.getCandidate(candidateId);
		responseInfo.setCandidate(candidate);
		responseInfo.setStatus(Constants.SUCCESS);
		responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
		responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
		return new ResponseEntity<ResponseInfo>(responseInfo, HttpStatus.OK);
	}

	@RequestMapping(value = "getVacancies/{userID}", method = RequestMethod.GET)
	public ResponseEntity<ResponseInfo> getVacanciesForEmployee(@PathVariable String userID) {
		ResponseInfo responseInfo = new ResponseInfo();
		List<VacancyRequest> vacancyList = recruitmentService.getVacanciesForEmployee(userID);
		responseInfo.setVacancyList(vacancyList);
		responseInfo.setStatus(Constants.SUCCESS);
		responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
		responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
		return new ResponseEntity<ResponseInfo>(responseInfo, HttpStatus.OK);
	}

	@RequestMapping(value = "getVacancies", method = RequestMethod.GET)
	public ResponseEntity<ResponseInfo> getVacanciesForHR() {
		ResponseInfo responseInfo = new ResponseInfo();
		List<VacancyRequest> vacancyList = recruitmentService.getVacanciesForHR();
		responseInfo.setVacancyList(vacancyList);
		responseInfo.setStatus(Constants.SUCCESS);
		responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
		responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
		return new ResponseEntity<ResponseInfo>(responseInfo, HttpStatus.OK);
	}

	@RequestMapping(value = "getVacanciesForUnitHead/{status}", method = RequestMethod.GET)
	public ResponseEntity<ResponseInfo> getVacanciesForUnitHead(
			@PathVariable String status) {
		ResponseInfo responseInfo = new ResponseInfo();
		List<VacancyRequest> vacancyList = recruitmentService
				.getVacanciesForUnitHead(status);
		responseInfo.setVacancyList(vacancyList);
		responseInfo.setStatus(Constants.SUCCESS);
		responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
		responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
		return new ResponseEntity<ResponseInfo>(responseInfo, HttpStatus.OK);
	}
	
	@RequestMapping(value = "getCandidatesForUnitHead/{status}", method = RequestMethod.GET)
	public ResponseEntity<ResponseInfo> getCandidatesForUnitHead(
			@PathVariable String status) {
		ResponseInfo responseInfo = new ResponseInfo();
		List<Candidate> candidateList = recruitmentService.getCandidatesForUnitHead(status);
		responseInfo.setCandidateList(candidateList);
		responseInfo.setStatus(Constants.SUCCESS);
		responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
		responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
		return new ResponseEntity<ResponseInfo>(responseInfo, HttpStatus.OK);
	}

	@RequestMapping(value = "saveVacancyRequest", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON, method = RequestMethod.POST)
	public ResponseInfo saveVacancyRequest(
			@RequestBody VacancyRequest vacancyRequest) {
		ResponseInfo responseInfo = new ResponseInfo();
		vacancyRequest = recruitmentService.saveVacancyRequest(vacancyRequest);
		if (vacancyRequest != null) {
			responseInfo.setStatus(Constants.SUCCESS);
			responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
			responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
		} else {
			responseInfo.setStatus(Constants.FAILURE);
			responseInfo.setErrorCode(Constants.FAILURE_ERROR_CD);
			responseInfo.setErrorMessage(Constants.saveErrorMessage);
		}
		return responseInfo;
	}

	@RequestMapping(value = "saveCandidate", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON, method = RequestMethod.POST)
	public ResponseInfo saveCandidate(@RequestBody Candidate candidate) {
		ResponseInfo responseInfo = new ResponseInfo();
		candidate = recruitmentService.saveCandidate(candidate);
		if (candidate != null) {
			responseInfo.setStatus(Constants.SUCCESS);
			responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
			responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
		} else {
			responseInfo.setStatus(Constants.FAILURE);
			responseInfo.setErrorCode(Constants.FAILURE_ERROR_CD);
			responseInfo.setErrorMessage(Constants.saveErrorMessage);
		}
		return responseInfo;
	}

	@RequestMapping(value = "saveResult", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON, method = RequestMethod.POST)
	public ResponseInfo saveMedicalResultByHR(@RequestBody Candidate candidate) {
		ResponseInfo responseInfo = new ResponseInfo();
		candidate = recruitmentService.saveMedicalResultByHR(candidate);
		if (candidate != null) {
			responseInfo.setStatus(Constants.SUCCESS);
			responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
			responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
		} else {
			responseInfo.setStatus(Constants.FAILURE);
			responseInfo.setErrorCode(Constants.FAILURE_ERROR_CD);
			responseInfo.setErrorMessage(Constants.saveErrorMessage);
		}
		return responseInfo;
	}

	@RequestMapping(value = "removeVacancyRequest", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON, method = RequestMethod.POST)
	public ResponseInfo removeVacancyRequest(
			@RequestBody VacancyRequest vacancyRequest) {
		ResponseInfo responseInfo = new ResponseInfo();
		try {
			recruitmentService.removeVacancyRequest(vacancyRequest);
			responseInfo.setStatus(Constants.SUCCESS);
			responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
			responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
		} catch (Exception e) {
			responseInfo.setStatus(Constants.FAILURE);
			responseInfo.setErrorCode(Constants.FAILURE_ERROR_CD);
			responseInfo.setErrorMessage(Constants.saveErrorMessage);
		}

		return responseInfo;
	}

	
	
	@RequestMapping(value = "apprOrRejUnitHeadCandidate", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON, method = RequestMethod.POST)
	public ResponseInfo apprOrRejUnitHeadCandidate(
			@RequestBody Candidate candidate) {
		ResponseInfo responseInfo = new ResponseInfo();
		try {
			recruitmentService.apprOrRejUnitHeadCandidate(candidate);
			responseInfo.setStatus(Constants.SUCCESS);
			responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
			responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
		} catch (Exception e) {
			responseInfo.setStatus(Constants.FAILURE);
			responseInfo.setErrorCode(Constants.FAILURE_ERROR_CD);
			responseInfo.setErrorMessage(Constants.saveErrorMessage);
		}
		return responseInfo;
	}

	
	
	@RequestMapping(value = "uploadFile",method = RequestMethod.POST)
	public  ResponseEntity<ResponseInfo> fileUpload(MultipartHttpServletRequest request) {
		ResponseInfo responseInfo = new ResponseInfo();
		try {
			Iterator<String> itr = request.getFileNames();
            @SuppressWarnings("unused")
			boolean emptyFile = true;
            FileUpload fileUpload = new FileUpload();
            
           while (itr.hasNext()) {
           	    emptyFile = false;
                String uploadedFile = itr.next();
                MultipartFile file = request.getFile(uploadedFile);
                fileUpload.setFile(file);
             	recruitmentService.fileUpload(fileUpload);
    			responseInfo.setStatus(Constants.SUCCESS);
    			responseInfo.setErrorCode(Constants.SUCCESS_ERROR_CD);
    			responseInfo.setErrorMessage(Constants.SUCCESS_ERROR_MSG);
            }
           
		} catch (Exception e) {
			responseInfo.setStatus(Constants.FAILURE);
			responseInfo.setErrorCode(Constants.FAILURE_ERROR_CD);
			responseInfo.setErrorMessage(Constants.saveErrorMessage);
		}

		  return new ResponseEntity<ResponseInfo>(responseInfo, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.CREATED)
	public void downloadDocx(@RequestParam ("name") String name,
	        HttpServletResponse response) throws Exception {
		try {
			String filePath = System.getProperty("catalina.home");
			System.out.println("Server path:" + filePath);
			File file = new File(filePath, name);
			InputStream fileInputStream = new FileInputStream(file);
	        OutputStream output = response.getOutputStream();
	
	        response.reset();
	
	        response.setContentType("application/octet-stream");
	        response.setContentLength((int) (file.length()));
	
	        response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
	
	        IOUtils.copyLarge(fileInputStream, output);
	        output.flush();
	    } catch (IOException e) {
	       
	    }
	}

}