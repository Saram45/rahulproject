myApp.service('hrRecService', ['$http','$q','$filter', function ($http, $q,$filter) {
      return{
    	   
				            
					            getVacanciesForUnitHead : function(status){
						            var getVacanciesUrl = "getVacanciesForUnitHead/"+status;
						            var deferred = $q.defer();
						        	 $http({
									        method: 'GET',
									        url: getVacanciesUrl,
									        headers: {'Content-Type': undefined},
									        transformRequest: angular.identity
									     })
								    .then(function(response,status) {
								    	var data = response.data;
								        if(data.errorCode == '200'){
					                       return deferred.resolve(data.vacancyList);
					                    }
					                    return deferred.resolve(data);
								     }, function(response){
						            	   return deferred.resolve(response);
							               });
						               return deferred.promise;
						            },
						            getVacancies : function(){
							            var getVacanciesUrl = "getVacancies";
							            var deferred = $q.defer();
							        	 $http({
										        method: 'GET',
										        url: getVacanciesUrl,
										        headers: {'Content-Type': undefined},
										        transformRequest: angular.identity
										     })
									    .then(function(response,status) {
									    	var data = response.data;
									        if(data.errorCode == '200'){
						                       return deferred.resolve(data.vacancyList);
						                    }
						                    return deferred.resolve(data);
									     }, function(response){
							            	   return deferred.resolve(response);
								               });
							               return deferred.promise;
							            },
				            
			           saveCandidate : function(candidateData){
				            var saveCandidateUrl = "saveCandidate";
				            var deferred = $q.defer();
					           $http.post(saveCandidateUrl, candidateData)
					   		    .then(function(response,status) {
							    	var data = response.data;
							        if(data.errorCode == '200'){
				                       return deferred.resolve(data);
				                    }
				                    return deferred.resolve(data);
							     }, function(response){
					            	   return deferred.resolve(response);
						               });
					               return deferred.promise;
				            }, 
				            getCandidates : function(candidateData){
					            var getCandidatesUrl = "getCandidates";
					            var deferred = $q.defer();
					        	 $http({
								        method: 'GET',
								        url: getCandidatesUrl,
								        headers: {'Content-Type': undefined},	
								        //data: formData,
								        transformRequest: angular.identity
								     })
							    .then(function(response,status) {
							    	var data = response.data;
							        if(data.errorCode == '200'){
				                       return deferred.resolve(data.candidateList);
				                    }
				                    return deferred.resolve(data);
							     }, function(response){
					            	   return deferred.resolve(response);
						               });
					               return deferred.promise;
					            },
					            getCandidate : function(candidate){
						            var getCandidateUrl = "getCandidate/"+candidate;
						            var deferred = $q.defer();
						        	 $http({
									        method: 'GET',
									        url: getCandidateUrl,
									        headers: {'Content-Type': undefined},
									        transformRequest: angular.identity
									     })
								    .then(function(response,status) {
								    	var data = response.data;
								        if(data.errorCode == '200'){
					                       return deferred.resolve(data.candidate);
					                    }
					                    return deferred.resolve(data);
								     }, function(response){
						            	   return deferred.resolve(response);
							               });
						               return deferred.promise;
						            },
					            
						            getEmployees : function(){
							            var getEmployees = "getEmployees";
							            var deferred = $q.defer();
							        	 $http({
										        method: 'GET',
										        url: getEmployees,
										        headers: {'Content-Type': undefined},
										        transformRequest: angular.identity
										     })
									    .then(function(response,status) {
									    	var data = response.data;
									        if(data.errorCode == '200'){
						                       return deferred.resolve(data.employeeList);
						                    }
						                    return deferred.resolve(data);
									     }, function(response){
							            	   return deferred.resolve(response);
								               });
							               return deferred.promise;
							            },
						            saveResult : function(candidateData){
							            var saveResultUrl = "saveResult";
							            var deferred = $q.defer();
								           $http.post(saveResultUrl, candidateData)
								   		    .then(function(response,status) {
										    	var data = response.data;
										        if(data.errorCode == '200'){
							                       return deferred.resolve(data);
							                    }
							                    return deferred.resolve(data);
										     }, function(response){
								            	   return deferred.resolve(response);
									               });
								               return deferred.promise;
							            },
			 uploadFile : function(file) {
					var uploadUrl = "uploadFile";
					var fd = new FormData();
					fd.append('file', file);
					var deferred = $q.defer();
					$http.post(uploadUrl, fd,{headers:{'Content-Type':undefined}})
					.then(function(response, status) {
						var data = response.data;
						if (data.errorCode == '200') {
							return deferred.resolve(data);
						}
						return deferred.resolve(data);
					}, function(response) {
						return deferred.resolve(response);
					});
					return deferred.promise;
				}
			
     	}
   }]);