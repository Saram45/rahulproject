myApp.service('recruitmentService', ['$http', '$q','$filter','$timeout', '$window', function ($http, $q,$filter,$timeout,$window) {
      return{
    	     title: function() { return title; },
    	     setTitle: function(newTitle) { title = newTitle },
    
             getProfiles : function(){
	            var getProfilesUrl = "getProfiles";
	            var deferred = $q.defer();
	        	 $http({
				        method: 'GET',
				        url: getProfilesUrl,
				        headers: {'Content-Type': undefined},	
				        transformRequest: angular.identity
				     })
			    .then(function(response,status) {
			    	var data = response.data;
			        if(data.errorCode == '200'){
                       return deferred.resolve(data.roleTypeList);
                    }
                    return deferred.resolve(data);
			     }, function(response){
	            	   return deferred.resolve(response);
		               });
	               return deferred.promise;
	            },
	            
	            getVacancies : function(loggedInUser){
		            var getVacanciesUrl = "getVacancies/"+loggedInUser;
		            var deferred = $q.defer();
		        	 $http({
					        method: 'GET',
					        url: getVacanciesUrl,
					        headers: {'Content-Type': undefined},	
					        transformRequest: angular.identity
					     })
				    .then(function(response,status) {
				    	var data = response.data;
				        if(data.errorCode == '200'){
	                       return deferred.resolve(data.vacancyList);
	                    }
	                    return deferred.resolve(data);
				     }, function(response){
		            	   return deferred.resolve(response);
			               });
		               return deferred.promise;
		            },
	            
	            saveVacancyRequest : function(vacancyData){
		            var saveVacancyReqUrl = "saveVacancyRequest";
		            var deferred = $q.defer();
		           $http.post(saveVacancyReqUrl, vacancyData)
		   		    .then(function(response,status) {
				    	var data = response.data;
				        if(data.errorCode == '200'){
	                       return deferred.resolve(data);
	                    }
	                    return deferred.resolve(data);
				     }, function(response){
		            	   return deferred.resolve(response);
			               });
		               return deferred.promise;
		            },
		            
		            removeVacancy : function(vacancyData){
			            var removeVacancyReqUrl = "removeVacancyRequest";
			            var deferred = $q.defer();
			           $http.post(removeVacancyReqUrl, vacancyData)
			   		    .then(function(response,status) {
					    	var data = response.data;
					        if(data.errorCode == '200'){
		                      console.log("Deleted successfully");
		                    }
		                   
					     }, function(response){
					    	 console.log("Error in deleting...");
				               });
			               return deferred.promise;
			            },
        
		            getVacanciesForUnitHead : function(status){
			            var getVacanciesUrl = "getVacanciesForUnitHead/"+status;
			            var deferred = $q.defer();
			        	 $http({
						        method: 'GET',
						        url: getVacanciesUrl,
						        headers: {'Content-Type': undefined},
						        transformRequest: angular.identity
						     })
					    .then(function(response,status) {
					    	var data = response.data;
					        if(data.errorCode == '200'){
		                       return deferred.resolve(data.vacancyList);
		                    }
		                    return deferred.resolve(data);
					     }, function(response){
			            	   return deferred.resolve(response);
				               });
			               return deferred.promise;
			            },
			            getCandidatesForUnitHead : function(status){
				            var getCandidatesUrl = "getCandidatesForUnitHead/"+status;
				            var deferred = $q.defer();
				        	 $http({
							        method: 'GET',
							        url: getCandidatesUrl,
							        headers: {'Content-Type': undefined},
							        transformRequest: angular.identity
							     })
						    .then(function(response,status) {
						    	var data = response.data;
						        if(data.errorCode == '200'){
			                       return deferred.resolve(data.candidateList);
			                    }
			                    return deferred.resolve(data);
						     }, function(response){
				            	   return deferred.resolve(response);
					               });
				               return deferred.promise;
				            },
		              
				            apprOrRejUnitHeadCandidate : function(candidateData,actionType){
					            var apprOrRejUnitHeadCandidate = "apprOrRejUnitHeadCandidate";
					            var deferred = $q.defer();
					            candidateData.actionType = actionType;
					           $http.post(apprOrRejUnitHeadCandidate, candidateData)
					   		    .then(function(response,status) {
							    	var data = response.data;
							        if(data.errorCode == '200'){
				                      console.log("Approved successfully");
				                    }
				                   
							     }, function(response){
							    	 console.log("Error in Approving or Rejecting...");
						               });
					               return deferred.promise;
					            },
								download : function(name) {
									var defer = $q.defer();
									$timeout(
											function() {
												$window.location = 'download?name='
														+ name;

											}, 1000).then(function() {
										defer.resolve('success');
									}, function() {
										defer.reject('error');
									});
									return defer.promise;
								}
		
     	}
   }]);