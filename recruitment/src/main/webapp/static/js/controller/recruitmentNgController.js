myApp
		.controller(
				'recruitmentCtrl',
				[
						'$scope',
						'$rootScope',
						'$http',
						'$q',
						'recruitmentService',
						'$filter',
						'$location',
						'$window',
						function($scope, $rootScope, $http, $q,
								recruitmentService, $filter, $location, $window) {

							$scope.errorMessage = false;

							$scope.login = function(Page) {
								$rootScope.loggedInUser = $scope.username;
								$rootScope.loggedInPassword = $scope.password;
								var getProfileUrl = "getProfile/"
										+ $rootScope.loggedInUser + "/"
										+ $rootScope.loggedInPassword;
								$scope.errorMessage = false;
								var deferred = $q.defer();
								$http({
									method : 'GET',
									url : getProfileUrl,
									headers : {
										'Content-Type' : undefined
									},
									transformRequest : angular.identity
								})
										.then(
												function(response, status) {
													var data = response.data;
													if (data.errorCode == '200') {
														if (data.role.roleType == "REG_EMPLOYEE") {
															console
																	.log("Regular");
															$location
																	.path("/employee");
														} else if (data.role.roleType == "UNIT_HEAD") {
															console
																	.log("UnitHead");
															$location
																	.path("/unitHead");
														} else if (data.role.roleType == "HR_REC_OFFICER") {
															console
																	.log("HR_REC_OFFICER");
															$location
																	.path("/hrRec");
														} else {
															$rootScope.loggedInUser = "";
															$scope.errorMessage = "User ID and Password are not matching. Please re-enter.";
														}
														return deferred
																.resolve(data.role);
													}
													return deferred
															.resolve(data.role);
												},
												function(response) {
													$scope.errorMessage = "User ID and Password are not matching. Please re-enter.";
													return deferred
															.resolve(response);
												});

							};

							$scope.logOut = function() {
								console.log("Inside logOut");
								$rootScope.loggedInUser = "";
								$rootScope.signOut = "";
								$window.location.href = "/recruitment/index.html";
							};

							$scope.backToTheEmployee = function() {
								console.log("screen is" + $scope.screenType
										+ "Root Screenis"
										+ $rootScope.screenType);
								if ($scope.screenType == 'employee') {
									$location.path("/employee");
								}
								if ($scope.screenType == 'unitHead') {
									$location.path("/unitHead");
								}
								if ($scope.screenType == 'hrRec') {
									$location.path("/hrRec");
								}

							};
  
							$scope.removeRow = function(vacancyIndex, vacancy) {

								recruitmentService.removeVacancy(vacancy).then(
										function(response) {

										}, function(response) {

										});
								$scope.vacancyList.splice(vacancyIndex, 1);
							};

							$scope.addVacancyRequest = function(screen) {
								$rootScope.screenType = screen;
								$scope.saveSuccessMessage = "";
								var getProfilesUrl = "getProfiles";
								recruitmentService.getProfiles().then(
										function(response)

										{
											$rootScope.roleTypeList = response;

										}, function(response) {

										});
								$location.path("/addVacancy");

							};

							$scope.viewVacancyRequest = function(screen) {
								$rootScope.screenType = screen;
								recruitmentService.getVacancies(
										$rootScope.loggedInUser).then(
										function(response) {
											$rootScope.vacancyList = response;

										}, function(response) {

										});
								$location.path("/viewVacancy");

							};
							
							$scope.saveVacancyRequest = function() {
								var vacancyData = {
									selectedRoleType : $scope.selectedRoleType,
									location : $scope.location,
									experience : $scope.experience,
									skill : $scope.skill,
									role : $scope.role,
									createdBy : $rootScope.loggedInUser
								};

								recruitmentService
										.saveVacancyRequest(vacancyData)
										.then(
												function(response)

												{
													if (response.errorCode == '200') {
														$scope.saveSuccessMessage = "Vacancy request has been successfully submitted";

													} else {
														$scope.saveSuccessMessage = "Data could not be saved.";

													}

												},
												function(response) {
													$scope.saveSuccessMessage = "Data could not be saved.";

												});

							};

							
							$scope.getVacanciesForUnitHead = function(screen,
									status) {
								$rootScope.screenType = screen;
								recruitmentService
										.getVacanciesForUnitHead(status)
										.then(
												function(response) {
													$rootScope.unitHeadvacancyList = response;
													$rootScope.unitHeadApproveVacancyList = response;

												}, function(response) {

												});
								if (status == "All") {
									$location.path("/unitHeadViewVacancy");
								}

							};
							$scope.getCandidatesForUnitHead = function(screen, status) {
								$rootScope.screenType = screen;
								$scope.saveSuccessMessage = "";
								recruitmentService.getCandidatesForUnitHead(status).then(
												function(response) {
													$rootScope.unitHeadCandidateList = response;
												}, function(response) {

												});
								if (status == "Approved") {
									$location.path("/unitHeadInterviewResultCandidate");
								} else {
									$location.path("/unitHeadViewAndApproveCandidate");
								}
							};
							$scope.removeRowUnitHead = function(
									unitHeadvacancyIndex, unitHeadvacancy) {

								recruitmentService.removeVacancy(
										unitHeadvacancy).then(
										function(response) {

										}, function(response) {

										});
								$scope.unitHeadvacancyList.splice(
										unitHeadvacancyIndex, 1);
							};

							
						
							$scope.apprOrRejUnitHeadCandidate = function(
									unitHeadCandidateIndex,
									unitHeadCandidate, actionType) {

								recruitmentService.apprOrRejUnitHeadCandidate(
										unitHeadCandidate, actionType)
										.then(function(response) {

										}, function(response) {

										});
								$scope.unitHeadCandidateList.splice(
										unitHeadCandidateIndex, 1);
							};
							
							$scope.download = function(fileName) {
								recruitmentService.download(fileName)
				                    .then(function(success) {
				                        console.log('success : ' + success);
				                    }, function(error) {
				                        console.log('error : ' + error);
				                    });
				            }; 
					

						
						} ]);
