myApp.controller(
				'hrRecCtrl',
				[
						'$scope',
						'$rootScope',
						'$http',
						'$q',
						'hrRecService',
						'$filter',
						'$location',
						'$window',
						function($scope, $rootScope, $http, $q,hrRecService, $filter, $location, $window) {
							
							$scope.addCandidate = function(screen, status) {
								$rootScope.screenType = screen;
								$scope.saveSuccessMessage = "";
								hrRecService.getVacanciesForUnitHead(status).then(function(response) {
									$rootScope.hrRecVacancyList = response;
								}, function(response) {

								});
								$location.path("/hrAddCandidate");

							};
							$scope.saveCandidate = function(vacancyReqId, name,age, sex, email) {
								var file = $scope.myFile;
								console.log('Save candidate file name is ' + file.name);
								var candidateData = {
									vacancyReqId : vacancyReqId,
									candidateName : name,
									candidateAge : age,
									candidateSex : sex,
									candidateEmail : email,
									resume : "/Resumes/"+file.name

								};
								hrRecService.saveCandidate(candidateData).then(function(response)

												{
													if (response.errorCode == '200') {
														$scope.saveCandidateSuccessMessage = "Candidate has been added successfully";

													} else {
														$scope.saveCandidateSuccessMessage = "Data could not be saved.";

													}

												},
												function(response) {
													$scope.saveCandidateSuccessMessage = "Data could not be saved.";

												});

							};
							$scope.testResults = function(screen) {
								$rootScope.screenType = screen;
								$scope.saveSuccessMessage = "";
								$rootScope.medicalClearedList = [ "Yes", "No" ];
								hrRecService.getCandidates().then(
												function(response) {
													$rootScope.candidateList = response;
												}, function(response) {

												});
								$location.path("/hrMedicalResultCandidate");

							};
							$scope.getCandidates = function(screen) {
								$rootScope.screenType = screen;
								$scope.saveSuccessMessage = "";
								hrRecService.getCandidates().then(
												function(response) {
													$rootScope.candidateList = response;
												}, function(response) {

												});
								$location.path("/hrViewCandidate");

							};
							$scope.candidateDropdown = function(candidate) {
								$scope.saveTestPageSuccessMessage = "";
								hrRecService.getCandidate(candidate).then(
												function(response) {
															$scope.candidateId = candidate,
															$scope.candidateName = response.candidateName,
															$scope.candidateAge = response.candidateAge,
															$scope.candidateSex = response.candidateSex,
															$scope.candidateEmail = response.candidateEmail,
															$scope.vacancyReqId = response.vacancyReqId,
															$scope.interviewCleared = response.interviewCleared

												}, function(response) {

												});
							};
							$scope.saveResult = function(selectedMedicalInd, candidateID) {
								var candidateData = {
									medicalCleared : selectedMedicalInd,
									candidateId : candidateID
								};
								hrRecService.saveResult(candidateData).then(
												function(response)

												{
													if (response.errorCode == '200') {
														$scope.saveTestResultSuccessMessage = "Test Result has been saved";

													} else {
														$scope.saveTestResultSuccessMessage = "Data could not be saved.";

													}

												},
												function(response) {
													$scope.saveSuccessMessage = "Data could not be saved.";

												});

							};
							/*$scope.getClearedCandidates = function(screen) {
								$rootScope.screenType = screen;
								hrRecService.getClearedCandidates().then(
												function(response) {
													$rootScope.hrClearedList = response;
												}, function(response) {

												});
								$location.path("/hrApproveCandidate");

							};*/
							$scope.getVacancies = function(screen) {
								$rootScope.screenType = screen;
								hrRecService.getVacancies().then(
												function(response) {
													$rootScope.vacancyList = response;
												}, function(response) {

												});
								$location.path("/viewVacancyWithStatus");

							};
							
							$scope.getEmployees = function(screen) {
								$rootScope.screenType = screen;
								hrRecService.getEmployees().then(
												function(response) {
													$rootScope.employeeList = response;
												}, function(response) {

												});
								$location.path("/hrViewEmployee");

							};
							/*$scope.approveCandidate = function(hrClearIndex,candidate) {

								hrRecService.approveCandidate(candidate).then(function(response) {

										}, function(response) {

										});
								$scope.hrClearedList.splice(hrClearIndex, 1);
							};*/
							$scope.backToTheEmployee = function() {
								console.log("screen is" + $scope.screenType
										+ "Root Screenis"
										+ $rootScope.screenType);
								if ($scope.screenType == 'employee') {
									$location.path("/employee");
								}
								if ($scope.screenType == 'unitHead') {
									$location.path("/unitHead");
								}
								if ($scope.screenType == 'hrRec') {
									$location.path("/hrRec");
								}

							};
						 $scope.uploadFile = function() {
								var file = $scope.myFile;
								console.log('file name is ' + file.name);
								console.dir(file);
								$scope.resumeUpload = "";
								hrRecService.uploadFile(file).then(
										function(response) {
											if(response.errorCode == '200'){
												$scope.resumeUpload = "Resume has been uploaded successfully!."
											}else{
												$scope.resumeUpload = "Error occured while uploading!."	
											}
										}, function(response) {

										});
						 };
						 
							
						} ]);