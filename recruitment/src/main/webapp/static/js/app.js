var myApp = angular.module('recruitmentApp', ["ngRoute"]);
myApp.config(function ($routeProvider) {
	   
    $routeProvider
     .when('/login', {
        templateUrl: '/recruitment/login.html',
        controller: 'recruitmentCtrl'
      })
     .when('/employee', {
        templateUrl: '/recruitment/employee.html',
        controller: 'recruitmentCtrl'
      })
    .when('/unitHead', {
        templateUrl: '/recruitment/unitHead.html',
        controller: 'recruitmentCtrl'
     })
    .when('/addVacancy', {
        templateUrl: '/recruitment/addVacancy.html',
        controller: 'recruitmentCtrl'
     })
    .when('/viewVacancy', {
        templateUrl: '/recruitment/viewVacancy.html',
        controller: 'recruitmentCtrl'
      })
      .when('/viewVacancyWithStatus', {
        templateUrl: '/recruitment/viewVacancyWithStatus.html',
        controller: 'recruitmentCtrl'
      })
    .when('/unitHeadViewVacancy', {
        templateUrl: '/recruitment/unitHeadViewVacancy.html',
        controller: 'recruitmentCtrl'
      })
     /*.when('/unitHeadApproveVacancy', {
        templateUrl: '/recruitment/unitHeadApproveVacancy.html',
        controller: 'recruitmentCtrl'
      })*/
    .when('/hrRec', {
        templateUrl: '/recruitment/hrRec.html',
        controller: 'hrRecCtrl'
     })
    .when('/hrAddCandidate', {
        templateUrl: '/recruitment/hrAddCandidate.html',
        controller: 'hrRecCtrl'
     })
    /*.when('/hrApproveCandidate', {
        templateUrl: '/recruitment/hrApproveCandidate.html',
        controller: 'hrRecCtrl'
     })*/
    .when('/hrMedicalResultCandidate', {
        templateUrl: '/recruitment/hrMedicalResultCandidate.html',
        controller: 'hrRecCtrl'
     })
     .when('/hrViewEmployee', {
        templateUrl: '/recruitment/hrViewEmployee.html',
        controller: 'hrRecCtrl'
     })
     .when('/hrViewCandidate', {
        templateUrl: '/recruitment/hrViewCandidate.html',
        controller: 'hrRecCtrl'
     })
     .when('/unitHeadViewAndApproveCandidate', {
        templateUrl: '/recruitment/unitHeadViewAndApproveCandidate.html',
        controller: 'recruitmentCtrl'
     })
     .when('/unitHeadInterviewResultCandidate', {
        templateUrl: '/recruitment/unitHeadInterviewResultCandidate.html',
        controller: 'recruitmentCtrl'
     })
    .otherwise( { redirectTo: "/login" });
});	

myApp.run(['$rootScope','$route', function($rootScope,$route) {
	
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
         if($rootScope.loggedInUser != "" && $rootScope.loggedInUser != undefined){
         $rootScope.signOut =  "Sign Out";
        }else{
        	$rootScope.signOut = "";	
        }
    });
}]);

myApp.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
          
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
}]);
 


     
